﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Estudios
    {
        private int _idestudio;
        private string _archivo;
        public int Idestudio { get => _idestudio; set => _idestudio = value; }
        public string Archivo { get => _archivo; set => _archivo = value; }
    }
}

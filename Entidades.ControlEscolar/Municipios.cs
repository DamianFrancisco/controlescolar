﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Municipios
    {
        private int _idMunicipio;
        private string _nombre;
        private string _fkestado;

        public int IdMunicipio { get => _idMunicipio; set => _idMunicipio = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Fkestado { get => _fkestado; set => _fkestado = value; }
    }
}

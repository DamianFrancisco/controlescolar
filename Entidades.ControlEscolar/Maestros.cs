﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Maestros
    {
        private string _nControl;
        private string _nombreM;
        private string _apellidopaternoM;
        private string _apellidomaternoM;
        private string _fechanacimiento;
        private string _estado;
        private string _ciudad;
        private string _sexo;
        private string _correo;
        private string _tarjeta;

        public string NControl { get => _nControl; set => _nControl = value; }
        public string NombreM { get => _nombreM; set => _nombreM = value; }
        public string ApellidopaternoM { get => _apellidopaternoM; set => _apellidopaternoM = value; }
        public string ApellidomaternoM { get => _apellidomaternoM; set => _apellidomaternoM = value; }
        public string Fechanacimiento { get => _fechanacimiento; set => _fechanacimiento = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Tarjeta { get => _tarjeta; set => _tarjeta = value; }
    }
}

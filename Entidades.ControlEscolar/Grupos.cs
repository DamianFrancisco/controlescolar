﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Grupos
    {
        private int _idgrupo;
        private string _grupo;
        private string _alumnos;
        private string _carrera;
        private string _materias;

        public int Idgrupo { get => _idgrupo; set => _idgrupo = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Alumnos { get => _alumnos; set => _alumnos = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Materias { get => _materias; set => _materias = value; }
    }
}

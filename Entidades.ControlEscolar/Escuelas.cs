﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Escuelas
    {
        private int _idescuela;
        private string _nombre;
        private int _numero;
        private string _estadoe;
        private string _municipioe;
        private string _telefono;
        private string _pagina;
        private string _email;
        private string _director;
        private string _logo;

        public int Idescuela { get => _idescuela; set => _idescuela = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public int Numero { get => _numero; set => _numero = value; }
        public string Estadoe { get => _estadoe; set => _estadoe = value; }
        public string Municipioe { get => _municipioe; set => _municipioe = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Pagina { get => _pagina; set => _pagina = value; }
        public string Email { get => _email; set => _email = value; }
        public string Director { get => _director; set => _director = value; }
        public string Logo { get => _logo; set => _logo = value; }

        public static implicit operator bool(Escuelas v)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Materias
    {
        private string _codigo;
        private string _materia;
        private string _carrera;
        private string _semestre;
        private int  _creditos;
        private string _rea;
        private string _reb;

        public string Codigo { get => _codigo; set => _codigo = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }
        public string Rea { get => _rea; set => _rea = value; }
        public string Reb { get => _reb; set => _reb = value; }
    }
}

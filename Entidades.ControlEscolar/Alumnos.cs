﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar
{
    public class Alumnos
    {
        private string _numeroControl;
        private string _nombre;
        private string _apellido_Paterno;
        private string _apellido_Materno;
        private string _sexo;
        private string _fecha_Nacimiento;
        private string _correo_Electronico;
        private string _telefono_Contacto;
        private string _estado;
        private string _municipio;
        private string _domicilio;

        public string NumeroControl { get => _numeroControl; set => _numeroControl = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellido_Paterno { get => _apellido_Paterno; set => _apellido_Paterno = value; }
        public string Apellido_Materno { get => _apellido_Materno; set => _apellido_Materno = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Fecha_Nacimiento { get => _fecha_Nacimiento; set => _fecha_Nacimiento = value; }
        public string Correo_Electronico { get => _correo_Electronico; set => _correo_Electronico = value; }
        public string Telefono_Contacto { get => _telefono_Contacto; set => _telefono_Contacto = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Municipio { get => _municipio; set => _municipio = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using Extensions.ControlEscolar;
using System.Text.RegularExpressions;
using System.IO;

namespace LogicaDeNegocio.ControlEscolar
{
    public class EscuelasManejador
    {
        private EscuelasAccesoDatos _escuelaAccesoDatos = new EscuelasAccesoDatos();
        private RutasManager _rutasManager;

        public EscuelasManejador(RutasManager rutasManager)
        {
            _rutasManager = rutasManager;
        }

        public void Eliminar(int idEscuela)
        {
            _escuelaAccesoDatos.Eliminar(idEscuela);
        }
        public void Guardar(Escuelas escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);
        }

        
        public Escuelas GetEscuela()
        {
            return _escuelaAccesoDatos.GetEscuela();
        }

        public bool CargarLogo(string filName)
        {
            var archivoNombre = new FileInfo(filName);

            if (archivoNombre.Length > 5000000)//cinco millones de bais 
            {
                return false;
            }
            return true;
        }

        public string GetNombreLogo(string fileName)
        {
            var archivoImagen = new FileInfo(fileName);
            return archivoImagen.Name;

        }

        public void LimpiarDocumento(int escuelaId, string tipoDocuemtno)
        {
            string rutaRepositorio = String.Empty;
            string _extension = String.Empty;
            switch (tipoDocuemtno)
            {
                case "png":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    _extension = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    _extension = "*.jpg";
                    break;
            }
            string ruta = Path.Combine(rutaRepositorio, escuelaId.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, _extension);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length != 0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);

                    if (archivoAnterior.Exists)
                    {
                        archivoAnterior.Delete();

                    }
                }
            }
        }
        public void GuardarLogo(string fileName, int escuelaId)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_rutasManager.RutaRepositoriosLogos, escuelaId.ToString());

                if (Directory.Exists(ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(ruta);
                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);

                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                        }
                    }
                    else
                    {
                        archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
                else
                {
                    _rutasManager.CrearRepositoriosLogosEscuela(escuelaId);
                    archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaDeNegocio.ControlEscolar
{
    public class EstudiosManejador
    {
        private EstudiosAccesoDatos _estudioAccesoDatos;

        public EstudiosManejador()
        {
            _estudioAccesoDatos = new EstudiosAccesoDatos();
        }
        public void Eliminar(string idEstudio)
        {
            _estudioAccesoDatos.Eliminar(idEstudio);
        }

        public void Guardar(Estudios estudio)
        {
            _estudioAccesoDatos.Guardar(estudio);
        }

        public List<Estudios> ObtenerLista(string filtro)
        {
            var list = new List<Estudios>();
            list = _estudioAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}

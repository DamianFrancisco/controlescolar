﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;


namespace LogicaDeNegocio.ControlEscolar
{
    public class GruposManejador
    {
        private GruposAccesoDatos _grupoAccesoDatos;
        public GruposManejador()
        {
            _grupoAccesoDatos = new GruposAccesoDatos();
        }
        public void Eliminar(int idGrupo)
        {
            _grupoAccesoDatos.Eliminar(idGrupo);
        }

        public void Guardar(Grupos grupo)
        {
            _grupoAccesoDatos.Guardar(grupo);
        }

        public List<Grupos> ObtenerLista(string filtro)
        {
            var list = new List<Grupos>();
            list = _grupoAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;


namespace LogicaDeNegocio.ControlEscolar
{
    public class MunicipiosManejador
    {
        private MunicipiosAccesoDatos _municipioAccesoDatos;
        public MunicipiosManejador()
        {
            _municipioAccesoDatos = new MunicipiosAccesoDatos();
        }

        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();
            list = _municipioAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}

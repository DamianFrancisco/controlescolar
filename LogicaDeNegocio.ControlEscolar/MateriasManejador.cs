﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaDeNegocio.ControlEscolar
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _materiaAccesoDatos;

        public MateriasManejador()
        {
            _materiaAccesoDatos = new MateriasAccesoDatos();
        }
        public void Eliminar(string Codigo)
        {
            _materiaAccesoDatos.Eliminar(Codigo);
        }

        public void Guardar(Materias materia)
        {
            _materiaAccesoDatos.Guardar(materia);
        }

        public List<Materias> ObtenerLista(string filtro)
        {
            var list = new List<Materias>();
            list = _materiaAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}

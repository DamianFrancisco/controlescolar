﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaDeNegocio.ControlEscolar
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos _estadoAccesoDatos;
        public EstadosManejador()
        {
            _estadoAccesoDatos = new EstadosAccesoDatos();
        }

        public List<Estados> ObtenerLista()
        {
            var list = new List<Estados>();
            list = _estadoAccesoDatos.ObtenerLista();
            return list;
        }
    }
}

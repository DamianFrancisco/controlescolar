﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaDeNegocio.ControlEscolar
{
    public class MaestrosManejador
    {
        private MaestrosAccesoDatos _maestroAccesoDatos;

        public MaestrosManejador()
        {
            _maestroAccesoDatos = new MaestrosAccesoDatos();
        }
        public void Eliminar(string ncontrol)
        {
            _maestroAccesoDatos.Eliminar(ncontrol);
        }

        public void Guardar(Maestros maestro)
        {
            _maestroAccesoDatos.Guardar(maestro);
        }

        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            list = _maestroAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}

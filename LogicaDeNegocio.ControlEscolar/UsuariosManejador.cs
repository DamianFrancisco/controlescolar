﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaDeNegocio.ControlEscolar
{
    public class UsuariosManejador
    {
        private UsuariosAccesoDatos _usuarioAccesoDatos;
        public UsuariosManejador()
        {
            _usuarioAccesoDatos = new UsuariosAccesoDatos();
        }
        public void Eliminar(int idUsuario)
        {
            _usuarioAccesoDatos.Eliminar(idUsuario);
        }

        public void Guardar(Usuarios usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);
        }

        private bool NommbreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool ApellidoPaternoValido(string apellidopaterno)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(apellidopaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool ApellidoMaternoValido(string apellidomaterno)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(apellidomaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public List<Usuarios> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            list = _usuarioAccesoDatos.ObtenerLista(filtro);
            return list;
        }

        public Tuple<bool, string> EsusuarioValido(Usuarios usuario)
        {
            string mensaje = " ";
            bool valido = true;

            if (usuario.Nombre.Length == 0)
            {
                mensaje = "El nombre de usuario es necesario";
                valido = false;
            }
            else if (NommbreValido(usuario.Nombre))
            {
                mensaje = "Escribre un formato valido para el nombre";
                valido = false;
            }
            else if (usuario.Nombre.Length > 15)
            {
                mensaje = "La longitud para nombre de usuario es maximo de 15 caracteres";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length == 0)
            {
                mensaje = "El apellido paterno de usuario es necesario";
                valido = false;
            }
            else if (ApellidoPaternoValido(usuario.ApellidoPaterno))
            {
                mensaje = "Escribe un formato valido para el apellido paterno";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length > 15)
            {
                mensaje = "La longitud para el apellido paterno de usuario es maximo de 15 caracteres";
                valido = false;
            }
            else if (usuario.ApellidoMaterno.Length == 0)
            {
                mensaje = "El apellido materno de usuario es necesario";
                valido = false;
            }
            else if (ApellidoMaternoValido(usuario.ApellidoMaterno))
            {
                mensaje = "Escribe un formato valido para el apellido materno";
                valido = false;
            }
            else if (usuario.ApellidoMaterno.Length > 15)
            {
                mensaje = "La longitud para el apellido materno de usuario es maximo de 15 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}

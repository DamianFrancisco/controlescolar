﻿namespace ControlEscolar
{
    partial class MateriasEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBuscar = new System.Windows.Forms.TextBox();
            this.DgvMaterias = new System.Windows.Forms.DataGridView();
            this.Btn_Materias = new Botones.BusEliNue();
            ((System.ComponentModel.ISupportInitialize)(this.DgvMaterias)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Location = new System.Drawing.Point(12, 25);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.Size = new System.Drawing.Size(232, 20);
            this.TxtBuscar.TabIndex = 0;
            // 
            // DgvMaterias
            // 
            this.DgvMaterias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvMaterias.Location = new System.Drawing.Point(12, 75);
            this.DgvMaterias.Name = "DgvMaterias";
            this.DgvMaterias.Size = new System.Drawing.Size(776, 205);
            this.DgvMaterias.TabIndex = 1;
            this.DgvMaterias.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvMaterias_CellDoubleClick);
            // 
            // Btn_Materias
            // 
            this.Btn_Materias.Location = new System.Drawing.Point(250, 12);
            this.Btn_Materias.Name = "Btn_Materias";
            this.Btn_Materias.Size = new System.Drawing.Size(245, 57);
            this.Btn_Materias.TabIndex = 2;
            this.Btn_Materias.TextButtonDelete = "";
            this.Btn_Materias.TextButtonNew = "";
            this.Btn_Materias.TextButtonSearch = "";
            this.Btn_Materias.EventSearch += new System.EventHandler(this.Btn_Materias_EventSearch);
            this.Btn_Materias.EventDelete += new System.EventHandler(this.Btn_Materias_EventDelete);
            this.Btn_Materias.EventNew += new System.EventHandler(this.Btn_Materias_EventNew);
            // 
            // MateriasEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(800, 289);
            this.Controls.Add(this.Btn_Materias);
            this.Controls.Add(this.DgvMaterias);
            this.Controls.Add(this.TxtBuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MateriasEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MATERIAS";
            this.Load += new System.EventHandler(this.MateriasEntrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvMaterias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtBuscar;
        private System.Windows.Forms.DataGridView DgvMaterias;
        private Botones.BusEliNue Btn_Materias;
    }
}
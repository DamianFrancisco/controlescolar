﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class AlumnosFormulario : Form
    {
        private AlumnosManejador _alumnoManejador;
        private Alumnos _alumno;
        private EstadosManejador _estadoManejador;
        private MunicipiosManejador _municipioManejador;
        private bool _isEnableBinding = false;
        public AlumnosFormulario()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnosManejador();
            _alumno = new Alumnos();
            _estadoManejador = new EstadosManejador();
            _municipioManejador = new MunicipiosManejador();
            _isEnableBinding = true;
            BindingAlumno();
        }

        public AlumnosFormulario(Alumnos alumno)
        {
            InitializeComponent();
            _alumnoManejador = new AlumnosManejador();
            _alumno = new Alumnos();
            _estadoManejador = new EstadosManejador();
            _municipioManejador = new MunicipiosManejador();
            _alumno = alumno;
            BindingAlumnoReload();
            _isEnableBinding = true;
        }

        private void AlumnosFormulario_Load(object sender, EventArgs e)
        {
            var Estados = _estadoManejador.ObtenerLista();
            foreach (var estado in Estados)
            {
                CmBE.Items.Add(estado.Nombre);
            }
        }
        private void BindingAlumnoReload()
        {
            TxtNControl.Text = _alumno.NumeroControl;
            TxtNombre.Text = _alumno.Nombre;
            TxtAPaterno.Text = _alumno.Apellido_Paterno;
            TxtAMaterno.Text = _alumno.Apellido_Materno;
            CmBS.Text = _alumno.Sexo;
            TxtFNacimiento.Text = _alumno.Fecha_Nacimiento;
            TxtCElectronico.Text = _alumno.Correo_Electronico;
            TxtTContacto.Text = _alumno.Telefono_Contacto;
            CmBE.Text = _alumno.Estado;
            CmBM.Text = _alumno.Municipio;
            TxtDomicilio.Text = _alumno.Domicilio;
        }

        private void BindingAlumno()
        {
            if (_isEnableBinding)
            {
                if (_alumno.NumeroControl == "")
                {
                    _alumno.NumeroControl = "";
                }

                _alumno.NumeroControl = TxtNControl.Text;
                _alumno.Nombre = TxtNombre.Text;
                _alumno.Apellido_Paterno = TxtAPaterno.Text;
                _alumno.Apellido_Materno = TxtAMaterno.Text;
                _alumno.Sexo = CmBS.Text;
                _alumno.Fecha_Nacimiento = TxtFNacimiento.Text;
                _alumno.Correo_Electronico = TxtCElectronico.Text;
                _alumno.Telefono_Contacto = TxtTContacto.Text;
                _alumno.Estado = CmBE.Text;
                _alumno.Municipio = CmBM.Text;
                _alumno.Domicilio = TxtDomicilio.Text;
            }
        }

        private void Guardar()
        {
            _alumnoManejador.Guardar(_alumno);
        }

        private bool ValidarAlumno()
        {   
            var res = _alumnoManejador.EsusuarioValidoA(_alumno);

            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void BtnAlumnosS_EventSave(object sender, EventArgs e)
        {
            BindingAlumno();

            if (ValidarAlumno())
            {
                Guardar();
                MessageBox.Show("Guardado Correctamente!!");
                this.Close();
            }
        }

        private void BtnAlumnosS_EventCancel(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmBE_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumno();
            CmBM.Items.Clear();
            string filtro = "";
            var Estados = _estadoManejador.ObtenerLista();
            foreach (var item in Estados)
            {
                if (item.Nombre.Equals(CmBE.Text))
                {
                    filtro = item.Codigo;
                }
            }
            var Municipios = _municipioManejador.ObtenerLista(filtro);
            foreach (var municipio in Municipios)
            {
                CmBM.Items.Add(municipio.Nombre);
            }
        }

        private void TxtNControl_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtAPaterno_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtAMaterno_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void CmBS_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtFNacimiento_ValueChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtCElectronico_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtTContacto_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void CmBM_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }

        private void TxtDomicilio_TextChanged(object sender, EventArgs e)
        {
            BindingAlumno();
        }
    }
}

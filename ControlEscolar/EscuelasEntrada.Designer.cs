﻿namespace ControlEscolar
{
    partial class EscuelasEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBuscar = new System.Windows.Forms.TextBox();
            this.DgvEscuelas = new System.Windows.Forms.DataGridView();
            this.Btn_Escuelas = new Botones.BusEliNue();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEscuelas)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Location = new System.Drawing.Point(12, 27);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.Size = new System.Drawing.Size(232, 20);
            this.TxtBuscar.TabIndex = 0;
            // 
            // DgvEscuelas
            // 
            this.DgvEscuelas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEscuelas.Location = new System.Drawing.Point(12, 75);
            this.DgvEscuelas.Name = "DgvEscuelas";
            this.DgvEscuelas.Size = new System.Drawing.Size(857, 205);
            this.DgvEscuelas.TabIndex = 1;
            this.DgvEscuelas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvEscuelas_CellDoubleClick);
            // 
            // Btn_Escuelas
            // 
            this.Btn_Escuelas.Location = new System.Drawing.Point(250, 12);
            this.Btn_Escuelas.Name = "Btn_Escuelas";
            this.Btn_Escuelas.Size = new System.Drawing.Size(245, 57);
            this.Btn_Escuelas.TabIndex = 2;
            this.Btn_Escuelas.TextButtonDelete = "";
            this.Btn_Escuelas.TextButtonNew = "";
            this.Btn_Escuelas.TextButtonSearch = "";
            this.Btn_Escuelas.EventSearch += new System.EventHandler(this.Btn_Escuelas_EventSearch);
            this.Btn_Escuelas.EventDelete += new System.EventHandler(this.Btn_Escuelas_EventDelete);
            this.Btn_Escuelas.EventNew += new System.EventHandler(this.Btn_Escuelas_EventNew);
            // 
            // EscuelasEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(881, 289);
            this.Controls.Add(this.Btn_Escuelas);
            this.Controls.Add(this.DgvEscuelas);
            this.Controls.Add(this.TxtBuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "EscuelasEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ESCUELAS";
            this.Load += new System.EventHandler(this.EscuelasEntrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvEscuelas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtBuscar;
        private System.Windows.Forms.DataGridView DgvEscuelas;
        private Botones.BusEliNue Btn_Escuelas;
    }
}
﻿namespace ControlEscolar
{
    partial class EstudiosFormulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtUniversidad = new System.Windows.Forms.TextBox();
            this.TxtMaestria = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPosgrado = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnEstudiosS = new Botones.GuaCan();
            this.BtnEstudios1 = new Botones.SelDel();
            this.BtnEsstudios2 = new Botones.SelDel();
            this.BtnEstudios3 = new Botones.SelDel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Universidad:";
            // 
            // TxtUniversidad
            // 
            this.TxtUniversidad.Location = new System.Drawing.Point(12, 25);
            this.TxtUniversidad.Name = "TxtUniversidad";
            this.TxtUniversidad.Size = new System.Drawing.Size(283, 20);
            this.TxtUniversidad.TabIndex = 1;
            // 
            // TxtMaestria
            // 
            this.TxtMaestria.Location = new System.Drawing.Point(12, 76);
            this.TxtMaestria.Name = "TxtMaestria";
            this.TxtMaestria.Size = new System.Drawing.Size(283, 20);
            this.TxtMaestria.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Maestria:";
            // 
            // TxtPosgrado
            // 
            this.TxtPosgrado.Location = new System.Drawing.Point(12, 128);
            this.TxtPosgrado.Name = "TxtPosgrado";
            this.TxtPosgrado.Size = new System.Drawing.Size(283, 20);
            this.TxtPosgrado.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Posgrado:";
            // 
            // BtnEstudiosS
            // 
            this.BtnEstudiosS.Location = new System.Drawing.Point(284, 174);
            this.BtnEstudiosS.Name = "BtnEstudiosS";
            this.BtnEstudiosS.Size = new System.Drawing.Size(170, 57);
            this.BtnEstudiosS.TabIndex = 6;
            this.BtnEstudiosS.TextButtonCancel = "";
            this.BtnEstudiosS.TextButtonSave = "";
            this.BtnEstudiosS.EventSave += new System.EventHandler(this.BtnEstudiosS_EventSave);
            this.BtnEstudiosS.EventCancel += new System.EventHandler(this.BtnEstudiosS_EventCancel);
            // 
            // BtnEstudios1
            // 
            this.BtnEstudios1.Location = new System.Drawing.Point(312, 9);
            this.BtnEstudios1.Name = "BtnEstudios1";
            this.BtnEstudios1.Size = new System.Drawing.Size(103, 42);
            this.BtnEstudios1.TabIndex = 7;
            this.BtnEstudios1.TextButtonDelete = "";
            this.BtnEstudios1.TextButtonSuccess = "";
            this.BtnEstudios1.EventSuccess += new System.EventHandler(this.BtnEstudios1_EventSuccess);
            this.BtnEstudios1.EventDelete += new System.EventHandler(this.BtnEstudios1_EventDelete);
            // 
            // BtnEsstudios2
            // 
            this.BtnEsstudios2.Location = new System.Drawing.Point(312, 60);
            this.BtnEsstudios2.Name = "BtnEsstudios2";
            this.BtnEsstudios2.Size = new System.Drawing.Size(103, 42);
            this.BtnEsstudios2.TabIndex = 8;
            this.BtnEsstudios2.TextButtonDelete = "";
            this.BtnEsstudios2.TextButtonSuccess = "";
            this.BtnEsstudios2.EventSuccess += new System.EventHandler(this.BtnEsstudios2_EventSuccess);
            this.BtnEsstudios2.EventDelete += new System.EventHandler(this.BtnEsstudios2_EventDelete);
            // 
            // BtnEstudios3
            // 
            this.BtnEstudios3.Location = new System.Drawing.Point(312, 112);
            this.BtnEstudios3.Name = "BtnEstudios3";
            this.BtnEstudios3.Size = new System.Drawing.Size(103, 42);
            this.BtnEstudios3.TabIndex = 9;
            this.BtnEstudios3.TextButtonDelete = "";
            this.BtnEstudios3.TextButtonSuccess = "";
            this.BtnEstudios3.EventSuccess += new System.EventHandler(this.BtnEstudios3_EventSuccess);
            this.BtnEstudios3.EventDelete += new System.EventHandler(this.BtnEstudios3_EventDelete);
            // 
            // EstudiosFormulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.ClientSize = new System.Drawing.Size(457, 234);
            this.Controls.Add(this.BtnEstudios3);
            this.Controls.Add(this.BtnEsstudios2);
            this.Controls.Add(this.BtnEstudios1);
            this.Controls.Add(this.BtnEstudiosS);
            this.Controls.Add(this.TxtPosgrado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtMaestria);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtUniversidad);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "EstudiosFormulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ESTUDIOS";
            this.Load += new System.EventHandler(this.EstudiosFormulario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtUniversidad;
        private System.Windows.Forms.TextBox TxtMaestria;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtPosgrado;
        private System.Windows.Forms.Label label3;
        private Botones.GuaCan BtnEstudiosS;
        private Botones.SelDel BtnEstudios1;
        private Botones.SelDel BtnEsstudios2;
        private Botones.SelDel BtnEstudios3;
    }
}
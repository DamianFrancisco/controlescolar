﻿namespace ControlEscolar
{
    partial class MaestrosFormulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaestrosFormulario));
            this.CmMBS = new System.Windows.Forms.ComboBox();
            this.CmMBC = new System.Windows.Forms.ComboBox();
            this.CmMBE = new System.Windows.Forms.ComboBox();
            this.TxtMFNacimiento = new System.Windows.Forms.DateTimePicker();
            this.TxtMMaterno = new System.Windows.Forms.TextBox();
            this.TxtMPaterno = new System.Windows.Forms.TextBox();
            this.TxtMNombre = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtMNcontrol = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNTar = new System.Windows.Forms.TextBox();
            this.TxtMCorreo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnAgregar = new Botones.ButtonDiferente();
            this.BtnMaestrosS = new Botones.GuaCan();
            this.SuspendLayout();
            // 
            // CmMBS
            // 
            this.CmMBS.FormattingEnabled = true;
            this.CmMBS.Items.AddRange(new object[] {
            "Femenino",
            "Masculino"});
            this.CmMBS.Location = new System.Drawing.Point(620, 137);
            this.CmMBS.Name = "CmMBS";
            this.CmMBS.Size = new System.Drawing.Size(109, 21);
            this.CmMBS.TabIndex = 73;
            this.CmMBS.SelectedIndexChanged += new System.EventHandler(this.CmMBS_SelectedIndexChanged);
            // 
            // CmMBC
            // 
            this.CmMBC.FormattingEnabled = true;
            this.CmMBC.Location = new System.Drawing.Point(425, 137);
            this.CmMBC.Name = "CmMBC";
            this.CmMBC.Size = new System.Drawing.Size(184, 21);
            this.CmMBC.TabIndex = 72;
            this.CmMBC.SelectedIndexChanged += new System.EventHandler(this.CmMBC_SelectedIndexChanged);
            // 
            // CmMBE
            // 
            this.CmMBE.FormattingEnabled = true;
            this.CmMBE.Location = new System.Drawing.Point(225, 137);
            this.CmMBE.Name = "CmMBE";
            this.CmMBE.Size = new System.Drawing.Size(184, 21);
            this.CmMBE.TabIndex = 71;
            this.CmMBE.SelectedIndexChanged += new System.EventHandler(this.CmMBE_SelectedIndexChanged);
            // 
            // TxtMFNacimiento
            // 
            this.TxtMFNacimiento.Location = new System.Drawing.Point(12, 137);
            this.TxtMFNacimiento.Name = "TxtMFNacimiento";
            this.TxtMFNacimiento.Size = new System.Drawing.Size(200, 20);
            this.TxtMFNacimiento.TabIndex = 70;
            this.TxtMFNacimiento.ValueChanged += new System.EventHandler(this.TxtMFNacimiento_ValueChanged);
            // 
            // TxtMMaterno
            // 
            this.TxtMMaterno.Location = new System.Drawing.Point(500, 89);
            this.TxtMMaterno.Name = "TxtMMaterno";
            this.TxtMMaterno.Size = new System.Drawing.Size(229, 20);
            this.TxtMMaterno.TabIndex = 69;
            this.TxtMMaterno.TextChanged += new System.EventHandler(this.TxtMMaterno_TextChanged);
            // 
            // TxtMPaterno
            // 
            this.TxtMPaterno.Location = new System.Drawing.Point(256, 89);
            this.TxtMPaterno.Name = "TxtMPaterno";
            this.TxtMPaterno.Size = new System.Drawing.Size(229, 20);
            this.TxtMPaterno.TabIndex = 68;
            this.TxtMPaterno.TextChanged += new System.EventHandler(this.TxtMPaterno_TextChanged);
            // 
            // TxtMNombre
            // 
            this.TxtMNombre.Location = new System.Drawing.Point(12, 89);
            this.TxtMNombre.Name = "TxtMNombre";
            this.TxtMNombre.Size = new System.Drawing.Size(229, 20);
            this.TxtMNombre.TabIndex = 67;
            this.TxtMNombre.TextChanged += new System.EventHandler(this.TxtMNombre_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(442, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 66;
            this.label10.Text = "Ciudad:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(242, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 65;
            this.label9.Text = "Estado:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 64;
            this.label6.Text = "Fecha Nacimiento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(637, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 63;
            this.label5.Text = "Sexo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(517, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 62;
            this.label4.Text = "Apellido Materno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Apellido Paterno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 60;
            this.label2.Text = "Nombre:";
            // 
            // TxtMNcontrol
            // 
            this.TxtMNcontrol.Location = new System.Drawing.Point(12, 41);
            this.TxtMNcontrol.Name = "TxtMNcontrol";
            this.TxtMNcontrol.Size = new System.Drawing.Size(184, 20);
            this.TxtMNcontrol.TabIndex = 75;
            this.TxtMNcontrol.TextChanged += new System.EventHandler(this.TxtMNcontrol_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 74;
            this.label1.Text = "Numero Control:";
            // 
            // TxtNTar
            // 
            this.TxtNTar.Location = new System.Drawing.Point(256, 187);
            this.TxtNTar.Name = "TxtNTar";
            this.TxtNTar.Size = new System.Drawing.Size(229, 20);
            this.TxtNTar.TabIndex = 79;
            this.TxtNTar.TextChanged += new System.EventHandler(this.TxtNTar_TextChanged);
            // 
            // TxtMCorreo
            // 
            this.TxtMCorreo.Location = new System.Drawing.Point(12, 187);
            this.TxtMCorreo.Name = "TxtMCorreo";
            this.TxtMCorreo.Size = new System.Drawing.Size(229, 20);
            this.TxtMCorreo.TabIndex = 78;
            this.TxtMCorreo.TextChanged += new System.EventHandler(this.TxtMCorreo_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(273, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 77;
            this.label8.Text = "Numero Tarjeta:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "Correo Electronico:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(508, 190);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 80;
            this.label13.Text = "Nivel de estudio:";
            // 
            // BtnAgregar
            // 
            this.BtnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("BtnAgregar.Image")));
            this.BtnAgregar.Location = new System.Drawing.Point(600, 177);
            this.BtnAgregar.Name = "BtnAgregar";
            this.BtnAgregar.Size = new System.Drawing.Size(71, 39);
            this.BtnAgregar.TabIndex = 81;
            this.BtnAgregar.UseVisualStyleBackColor = true;
            this.BtnAgregar.Click += new System.EventHandler(this.BtnAgregar_Click);
            // 
            // BtnMaestrosS
            // 
            this.BtnMaestrosS.Location = new System.Drawing.Point(559, 224);
            this.BtnMaestrosS.Name = "BtnMaestrosS";
            this.BtnMaestrosS.Size = new System.Drawing.Size(170, 57);
            this.BtnMaestrosS.TabIndex = 82;
            this.BtnMaestrosS.TextButtonCancel = "";
            this.BtnMaestrosS.TextButtonSave = "";
            this.BtnMaestrosS.EventSave += new System.EventHandler(this.guaCan1_EventSave);
            this.BtnMaestrosS.EventCancel += new System.EventHandler(this.guaCan1_EventCancel);
            // 
            // MaestrosFormulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(744, 293);
            this.Controls.Add(this.BtnMaestrosS);
            this.Controls.Add(this.BtnAgregar);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.TxtNTar);
            this.Controls.Add(this.TxtMCorreo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtMNcontrol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmMBS);
            this.Controls.Add(this.CmMBC);
            this.Controls.Add(this.CmMBE);
            this.Controls.Add(this.TxtMFNacimiento);
            this.Controls.Add(this.TxtMMaterno);
            this.Controls.Add(this.TxtMPaterno);
            this.Controls.Add(this.TxtMNombre);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MaestrosFormulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MaestrosFormulario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmMBS;
        private System.Windows.Forms.ComboBox CmMBC;
        private System.Windows.Forms.ComboBox CmMBE;
        private System.Windows.Forms.DateTimePicker TxtMFNacimiento;
        private System.Windows.Forms.TextBox TxtMMaterno;
        private System.Windows.Forms.TextBox TxtMPaterno;
        private System.Windows.Forms.TextBox TxtMNombre;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtMNcontrol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNTar;
        private System.Windows.Forms.TextBox TxtMCorreo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private Botones.ButtonDiferente BtnAgregar;
        private Botones.GuaCan BtnMaestrosS;
    }
}
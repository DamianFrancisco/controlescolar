﻿namespace ControlEscolar
{
    partial class MaestrosEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBuscar = new System.Windows.Forms.TextBox();
            this.DgvMaestros = new System.Windows.Forms.DataGridView();
            this.Btn_Maestros = new Botones.BusEliNue();
            ((System.ComponentModel.ISupportInitialize)(this.DgvMaestros)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Location = new System.Drawing.Point(12, 27);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.Size = new System.Drawing.Size(232, 20);
            this.TxtBuscar.TabIndex = 0;
            // 
            // DgvMaestros
            // 
            this.DgvMaestros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvMaestros.Location = new System.Drawing.Point(12, 75);
            this.DgvMaestros.Name = "DgvMaestros";
            this.DgvMaestros.Size = new System.Drawing.Size(851, 205);
            this.DgvMaestros.TabIndex = 1;
            this.DgvMaestros.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvMaestros_CellDoubleClick);
            // 
            // Btn_Maestros
            // 
            this.Btn_Maestros.Location = new System.Drawing.Point(250, 12);
            this.Btn_Maestros.Name = "Btn_Maestros";
            this.Btn_Maestros.Size = new System.Drawing.Size(245, 57);
            this.Btn_Maestros.TabIndex = 2;
            this.Btn_Maestros.TextButtonDelete = "";
            this.Btn_Maestros.TextButtonNew = "";
            this.Btn_Maestros.TextButtonSearch = "";
            this.Btn_Maestros.EventSearch += new System.EventHandler(this.Btn_Maestros_EventSearch);
            this.Btn_Maestros.EventDelete += new System.EventHandler(this.Btn_Maestros_EventDelete);
            this.Btn_Maestros.EventNew += new System.EventHandler(this.Btn_Maestros_EventNew);
            // 
            // MaestrosEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(875, 287);
            this.Controls.Add(this.Btn_Maestros);
            this.Controls.Add(this.DgvMaestros);
            this.Controls.Add(this.TxtBuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MaestrosEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MAESTROS";
            this.Load += new System.EventHandler(this.MaestrosEntrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvMaestros)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtBuscar;
        private System.Windows.Forms.DataGridView DgvMaestros;
        private Botones.BusEliNue Btn_Maestros;
    }
}
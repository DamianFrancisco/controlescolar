﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void uSUARIOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsuariosEntrada frmUsuarios = new UsuariosEntrada();
            frmUsuarios.ShowDialog();
        }

        private void aLUMNOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AlumnosEntrada frmAlumnos = new AlumnosEntrada();
            frmAlumnos.ShowDialog();
        }

        private void mAESTROSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaestrosEntrada frmMaestros = new MaestrosEntrada();
            frmMaestros.ShowDialog();
        }

        private void eSCUELASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EscuelasEntrada frmEscuelas = new EscuelasEntrada();
            frmEscuelas.ShowDialog();
        }
    }
}

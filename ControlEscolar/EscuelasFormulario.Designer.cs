﻿namespace ControlEscolar
{
    partial class EscuelasFormulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EscuelasFormulario));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtNumero = new System.Windows.Forms.TextBox();
            this.CmBE = new System.Windows.Forms.ComboBox();
            this.CmBM = new System.Windows.Forms.ComboBox();
            this.TxtTelefono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtPagina = new System.Windows.Forms.TextBox();
            this.TxtEmail = new System.Windows.Forms.TextBox();
            this.TxtDirector = new System.Windows.Forms.TextBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.BtnEscuelasS = new Botones.GuaCan();
            this.BtnEscuelas = new Botones.SelDel();
            this.BtnModificar = new Botones.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numero Exterior:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Estado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Municipio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Telefono:";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(12, 40);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(232, 20);
            this.TxtNombre.TabIndex = 14;
            // 
            // TxtNumero
            // 
            this.TxtNumero.Location = new System.Drawing.Point(250, 40);
            this.TxtNumero.Name = "TxtNumero";
            this.TxtNumero.Size = new System.Drawing.Size(117, 20);
            this.TxtNumero.TabIndex = 15;
            // 
            // CmBE
            // 
            this.CmBE.FormattingEnabled = true;
            this.CmBE.Location = new System.Drawing.Point(12, 91);
            this.CmBE.Name = "CmBE";
            this.CmBE.Size = new System.Drawing.Size(211, 21);
            this.CmBE.TabIndex = 16;
            // 
            // CmBM
            // 
            this.CmBM.FormattingEnabled = true;
            this.CmBM.Location = new System.Drawing.Point(229, 91);
            this.CmBM.Name = "CmBM";
            this.CmBM.Size = new System.Drawing.Size(211, 21);
            this.CmBM.TabIndex = 17;
            // 
            // TxtTelefono
            // 
            this.TxtTelefono.Location = new System.Drawing.Point(12, 142);
            this.TxtTelefono.Name = "TxtTelefono";
            this.TxtTelefono.Size = new System.Drawing.Size(144, 20);
            this.TxtTelefono.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Pagina Web:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Email Principal:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(255, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Director:";
            // 
            // TxtPagina
            // 
            this.TxtPagina.Location = new System.Drawing.Point(162, 142);
            this.TxtPagina.Name = "TxtPagina";
            this.TxtPagina.Size = new System.Drawing.Size(278, 20);
            this.TxtPagina.TabIndex = 34;
            // 
            // TxtEmail
            // 
            this.TxtEmail.Location = new System.Drawing.Point(12, 196);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Size = new System.Drawing.Size(217, 20);
            this.TxtEmail.TabIndex = 35;
            // 
            // TxtDirector
            // 
            this.TxtDirector.Location = new System.Drawing.Point(235, 196);
            this.TxtDirector.Name = "TxtDirector";
            this.TxtDirector.Size = new System.Drawing.Size(205, 20);
            this.TxtDirector.TabIndex = 36;
            // 
            // picLogo
            // 
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picLogo.Location = new System.Drawing.Point(469, 24);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(305, 224);
            this.picLogo.TabIndex = 37;
            this.picLogo.TabStop = false;
            // 
            // BtnEscuelasS
            // 
            this.BtnEscuelasS.Location = new System.Drawing.Point(229, 254);
            this.BtnEscuelasS.Name = "BtnEscuelasS";
            this.BtnEscuelasS.Size = new System.Drawing.Size(170, 57);
            this.BtnEscuelasS.TabIndex = 38;
            this.BtnEscuelasS.TextButtonCancel = "";
            this.BtnEscuelasS.TextButtonSave = "";
            this.BtnEscuelasS.EventSave += new System.EventHandler(this.BtnEscuelasS_EventSave);
            this.BtnEscuelasS.EventCancel += new System.EventHandler(this.BtnEscuelasS_EventCancel);
            // 
            // BtnEscuelas
            // 
            this.BtnEscuelas.Location = new System.Drawing.Point(671, 254);
            this.BtnEscuelas.Name = "BtnEscuelas";
            this.BtnEscuelas.Size = new System.Drawing.Size(103, 42);
            this.BtnEscuelas.TabIndex = 39;
            this.BtnEscuelas.TextButtonDelete = "";
            this.BtnEscuelas.TextButtonSuccess = "";
            this.BtnEscuelas.EventSuccess += new System.EventHandler(this.BtnEscuelas_EventSuccess);
            this.BtnEscuelas.EventDelete += new System.EventHandler(this.BtnEscuelas_EventDelete);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Image = ((System.Drawing.Image)(resources.GetObject("BtnModificar.Image")));
            this.BtnModificar.Location = new System.Drawing.Point(134, 254);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(89, 57);
            this.BtnModificar.TabIndex = 40;
            this.BtnModificar.UseVisualStyleBackColor = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // EscuelasFormulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(797, 320);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.BtnEscuelas);
            this.Controls.Add(this.BtnEscuelasS);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.TxtDirector);
            this.Controls.Add(this.TxtEmail);
            this.Controls.Add(this.TxtPagina);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtTelefono);
            this.Controls.Add(this.CmBM);
            this.Controls.Add(this.CmBE);
            this.Controls.Add(this.TxtNumero);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EscuelasFormulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.EscuelasFormulario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtNumero;
        private System.Windows.Forms.ComboBox CmBE;
        private System.Windows.Forms.ComboBox CmBM;
        private System.Windows.Forms.TextBox TxtTelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtPagina;
        private System.Windows.Forms.TextBox TxtEmail;
        private System.Windows.Forms.TextBox TxtDirector;
        private System.Windows.Forms.PictureBox picLogo;
        private Botones.GuaCan BtnEscuelasS;
        private Botones.SelDel BtnEscuelas;
        private Botones.ButtonEdit BtnModificar;
    }
}
﻿namespace ControlEscolar
{
    partial class AlumnosFormulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtNControl = new System.Windows.Forms.TextBox();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtAPaterno = new System.Windows.Forms.TextBox();
            this.TxtAMaterno = new System.Windows.Forms.TextBox();
            this.CmBS = new System.Windows.Forms.ComboBox();
            this.TxtFNacimiento = new System.Windows.Forms.DateTimePicker();
            this.TxtCElectronico = new System.Windows.Forms.TextBox();
            this.TxtTContacto = new System.Windows.Forms.TextBox();
            this.CmBE = new System.Windows.Forms.ComboBox();
            this.CmBM = new System.Windows.Forms.ComboBox();
            this.TxtDomicilio = new System.Windows.Forms.TextBox();
            this.BtnAlumnosS = new Botones.GuaCan();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero Control:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(259, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Paterno:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(504, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido Materno:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sexo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(148, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fecha Nacimiento:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(350, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Correo Electronico:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(587, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Telefono Contacto:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Estado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(247, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Municipio:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(463, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Domicilio:";
            // 
            // TxtNControl
            // 
            this.TxtNControl.Location = new System.Drawing.Point(12, 37);
            this.TxtNControl.Name = "TxtNControl";
            this.TxtNControl.Size = new System.Drawing.Size(199, 20);
            this.TxtNControl.TabIndex = 12;
            this.TxtNControl.TextChanged += new System.EventHandler(this.TxtNControl_TextChanged);
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(12, 87);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(232, 20);
            this.TxtNombre.TabIndex = 13;
            this.TxtNombre.TextChanged += new System.EventHandler(this.TxtNombre_TextChanged);
            // 
            // TxtAPaterno
            // 
            this.TxtAPaterno.Location = new System.Drawing.Point(250, 87);
            this.TxtAPaterno.Name = "TxtAPaterno";
            this.TxtAPaterno.Size = new System.Drawing.Size(232, 20);
            this.TxtAPaterno.TabIndex = 14;
            this.TxtAPaterno.TextChanged += new System.EventHandler(this.TxtAPaterno_TextChanged);
            // 
            // TxtAMaterno
            // 
            this.TxtAMaterno.Location = new System.Drawing.Point(488, 87);
            this.TxtAMaterno.Name = "TxtAMaterno";
            this.TxtAMaterno.Size = new System.Drawing.Size(232, 20);
            this.TxtAMaterno.TabIndex = 15;
            this.TxtAMaterno.TextChanged += new System.EventHandler(this.TxtAMaterno_TextChanged);
            // 
            // CmBS
            // 
            this.CmBS.FormattingEnabled = true;
            this.CmBS.Items.AddRange(new object[] {
            "Femenino",
            "Masculino"});
            this.CmBS.Location = new System.Drawing.Point(12, 137);
            this.CmBS.Name = "CmBS";
            this.CmBS.Size = new System.Drawing.Size(111, 21);
            this.CmBS.TabIndex = 26;
            this.CmBS.SelectedIndexChanged += new System.EventHandler(this.CmBS_SelectedIndexChanged);
            // 
            // TxtFNacimiento
            // 
            this.TxtFNacimiento.Location = new System.Drawing.Point(129, 138);
            this.TxtFNacimiento.Name = "TxtFNacimiento";
            this.TxtFNacimiento.Size = new System.Drawing.Size(200, 20);
            this.TxtFNacimiento.TabIndex = 27;
            this.TxtFNacimiento.ValueChanged += new System.EventHandler(this.TxtFNacimiento_ValueChanged);
            // 
            // TxtCElectronico
            // 
            this.TxtCElectronico.Location = new System.Drawing.Point(335, 137);
            this.TxtCElectronico.Name = "TxtCElectronico";
            this.TxtCElectronico.Size = new System.Drawing.Size(232, 20);
            this.TxtCElectronico.TabIndex = 28;
            this.TxtCElectronico.TextChanged += new System.EventHandler(this.TxtCElectronico_TextChanged);
            // 
            // TxtTContacto
            // 
            this.TxtTContacto.Location = new System.Drawing.Point(573, 137);
            this.TxtTContacto.Name = "TxtTContacto";
            this.TxtTContacto.Size = new System.Drawing.Size(144, 20);
            this.TxtTContacto.TabIndex = 29;
            this.TxtTContacto.TextChanged += new System.EventHandler(this.TxtTContacto_TextChanged);
            // 
            // CmBE
            // 
            this.CmBE.FormattingEnabled = true;
            this.CmBE.Location = new System.Drawing.Point(12, 189);
            this.CmBE.Name = "CmBE";
            this.CmBE.Size = new System.Drawing.Size(211, 21);
            this.CmBE.TabIndex = 30;
            this.CmBE.SelectedIndexChanged += new System.EventHandler(this.CmBE_SelectedIndexChanged);
            // 
            // CmBM
            // 
            this.CmBM.FormattingEnabled = true;
            this.CmBM.Location = new System.Drawing.Point(229, 189);
            this.CmBM.Name = "CmBM";
            this.CmBM.Size = new System.Drawing.Size(211, 21);
            this.CmBM.TabIndex = 31;
            this.CmBM.SelectedIndexChanged += new System.EventHandler(this.CmBM_SelectedIndexChanged);
            // 
            // TxtDomicilio
            // 
            this.TxtDomicilio.Location = new System.Drawing.Point(446, 189);
            this.TxtDomicilio.Name = "TxtDomicilio";
            this.TxtDomicilio.Size = new System.Drawing.Size(270, 20);
            this.TxtDomicilio.TabIndex = 32;
            this.TxtDomicilio.TextChanged += new System.EventHandler(this.TxtDomicilio_TextChanged);
            // 
            // BtnAlumnosS
            // 
            this.BtnAlumnosS.Location = new System.Drawing.Point(547, 231);
            this.BtnAlumnosS.Name = "BtnAlumnosS";
            this.BtnAlumnosS.Size = new System.Drawing.Size(170, 57);
            this.BtnAlumnosS.TabIndex = 33;
            this.BtnAlumnosS.TextButtonCancel = "";
            this.BtnAlumnosS.TextButtonSave = "";
            this.BtnAlumnosS.EventSave += new System.EventHandler(this.BtnAlumnosS_EventSave);
            this.BtnAlumnosS.EventCancel += new System.EventHandler(this.BtnAlumnosS_EventCancel);
            // 
            // AlumnosFormulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(733, 295);
            this.Controls.Add(this.BtnAlumnosS);
            this.Controls.Add(this.TxtDomicilio);
            this.Controls.Add(this.CmBM);
            this.Controls.Add(this.CmBE);
            this.Controls.Add(this.TxtTContacto);
            this.Controls.Add(this.TxtCElectronico);
            this.Controls.Add(this.TxtFNacimiento);
            this.Controls.Add(this.CmBS);
            this.Controls.Add(this.TxtAMaterno);
            this.Controls.Add(this.TxtAPaterno);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.TxtNControl);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AlumnosFormulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AlumnosFormulario";
            this.Load += new System.EventHandler(this.AlumnosFormulario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TxtNControl;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtAPaterno;
        private System.Windows.Forms.TextBox TxtAMaterno;
        private System.Windows.Forms.ComboBox CmBS;
        private System.Windows.Forms.DateTimePicker TxtFNacimiento;
        private System.Windows.Forms.TextBox TxtCElectronico;
        private System.Windows.Forms.TextBox TxtTContacto;
        private System.Windows.Forms.ComboBox CmBE;
        private System.Windows.Forms.ComboBox CmBM;
        private System.Windows.Forms.TextBox TxtDomicilio;
        private Botones.GuaCan BtnAlumnosS;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;


namespace ControlEscolar
{
    public partial class MaestrosFormulario : Form
    {
        private MaestrosManejador _maestroManejador;
        private Maestros _maestro;
        private EstadosManejador _estadoManejador;
        private MunicipiosManejador _municipioManejador;
        private bool _isEnableBinding = false;
        public MaestrosFormulario()
        {
            InitializeComponent();
            _maestroManejador = new MaestrosManejador();
            _maestro = new Maestros();
            _estadoManejador = new EstadosManejador();
            _municipioManejador = new MunicipiosManejador();
            _isEnableBinding = true;
            BindingMaestro();
        }

        public MaestrosFormulario(Maestros maestro)
        {
            InitializeComponent();
            _maestroManejador = new MaestrosManejador();
            _maestro = new Maestros();
            _estadoManejador = new EstadosManejador();
            _municipioManejador = new MunicipiosManejador();
            _maestro = maestro;
            BindingMaestroReload();
            _isEnableBinding = true;
        }

        private void BindingMaestroReload()
        {
            TxtMNcontrol.Text = _maestro.NControl;
            TxtMNombre.Text = _maestro.NombreM;
            TxtMPaterno.Text = _maestro.ApellidopaternoM;
            TxtMMaterno.Text = _maestro.ApellidomaternoM;
            TxtMFNacimiento.Text = _maestro.Fechanacimiento;
            CmMBE.Text = _maestro.Estado;
            CmMBC.Text = _maestro.Ciudad;
            CmMBS.Text = _maestro.Sexo;
            TxtMCorreo.Text = _maestro.Correo;
            TxtNTar.Text = _maestro.Tarjeta;
        }

        private void BindingMaestro()
        {
            if (_isEnableBinding)
            {
                if (_maestro.NControl != "")
                {
                    _maestro.NControl = "";
                }

                _maestro.NControl = TxtMNcontrol.Text;
                _maestro.NombreM = TxtMNombre.Text;
                _maestro.ApellidopaternoM = TxtMPaterno.Text;
                _maestro.ApellidomaternoM = TxtMMaterno.Text;
                _maestro.Fechanacimiento = TxtMFNacimiento.Text;
                _maestro.Estado = CmMBE.Text;
                _maestro.Ciudad = CmMBC.Text;
                _maestro.Sexo = CmMBS.Text;
                _maestro.Correo = TxtMCorreo.Text;
                _maestro.Tarjeta = TxtNTar.Text;
            }

        }

        private void Guardar()
        {
            _maestroManejador.Guardar(_maestro);
        }
        private void MaestrosFormulario_Load(object sender, EventArgs e)
        {
            var Estados = _estadoManejador.ObtenerLista();
            foreach (var estado in Estados)
            {
                CmMBE.Items.Add(estado.Nombre);
            }
        }

        private void guaCan1_EventSave(object sender, EventArgs e)
        {
            BindingMaestro();
            Guardar();
            MessageBox.Show("Guardado Correctamente!!");
            this.Close();
        }

        private void guaCan1_EventCancel(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            EstudiosFormulario frmEstudios = new EstudiosFormulario();
            frmEstudios.ShowDialog();
        }

        private void CmMBE_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingMaestro();
            CmMBC.Items.Clear();
            string filtro = "";
            var Estados = _estadoManejador.ObtenerLista();
            foreach (var item in Estados)
            {
                if (item.Nombre.Equals(CmMBC.Text))
                {
                    filtro = item.Codigo;
                }
            }
            var Municipios = _municipioManejador.ObtenerLista(filtro);
            foreach (var municipio in Municipios)
            {
                CmMBC.Items.Add(municipio.Nombre);
            }
        }

        private void TxtMNcontrol_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtMNombre_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtMPaterno_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtMMaterno_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtMFNacimiento_ValueChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void CmMBC_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void CmMBS_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtMCorreo_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }

        private void TxtNTar_TextChanged(object sender, EventArgs e)
        {
            BindingMaestro();
        }
    }
}

﻿namespace ControlEscolar
{
    partial class AlumnosEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvAlumnos = new System.Windows.Forms.DataGridView();
            this.Btn_Alumnos = new Botones.BusEliNue();
            this.TxtBuscar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAlumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvAlumnos
            // 
            this.DgvAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAlumnos.Location = new System.Drawing.Point(6, 70);
            this.DgvAlumnos.Name = "DgvAlumnos";
            this.DgvAlumnos.Size = new System.Drawing.Size(857, 205);
            this.DgvAlumnos.TabIndex = 0;
            this.DgvAlumnos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAlumnos_CellDoubleClick);
            // 
            // Btn_Alumnos
            // 
            this.Btn_Alumnos.Location = new System.Drawing.Point(244, 7);
            this.Btn_Alumnos.Name = "Btn_Alumnos";
            this.Btn_Alumnos.Size = new System.Drawing.Size(245, 57);
            this.Btn_Alumnos.TabIndex = 1;
            this.Btn_Alumnos.TextButtonDelete = "";
            this.Btn_Alumnos.TextButtonNew = "";
            this.Btn_Alumnos.TextButtonSearch = "";
            this.Btn_Alumnos.EventSearch += new System.EventHandler(this.Btn_Alumnos_EventSearch);
            this.Btn_Alumnos.EventDelete += new System.EventHandler(this.Btn_Alumnos_EventDelete);
            this.Btn_Alumnos.EventNew += new System.EventHandler(this.Btn_Alumnos_EventNew);
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Location = new System.Drawing.Point(6, 27);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.Size = new System.Drawing.Size(232, 20);
            this.TxtBuscar.TabIndex = 2;
            this.TxtBuscar.TextChanged += new System.EventHandler(this.TxtBuscar_TextChanged);
            // 
            // AlumnosEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.YellowGreen;
            this.ClientSize = new System.Drawing.Size(875, 283);
            this.Controls.Add(this.TxtBuscar);
            this.Controls.Add(this.Btn_Alumnos);
            this.Controls.Add(this.DgvAlumnos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "AlumnosEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ALUMNOS";
            this.Load += new System.EventHandler(this.AlumnosEntrada_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvAlumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvAlumnos;
        private Botones.BusEliNue Btn_Alumnos;
        private System.Windows.Forms.TextBox TxtBuscar;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class MaestrosEntrada : Form
    {
        MaestrosManejador _maestroManejador;
        Maestros _maestro;
        public MaestrosEntrada()
        {
            InitializeComponent();
            _maestroManejador = new MaestrosManejador();
            _maestro = new Maestros();
        }

        private void BuscarMaestros(string filtro)
        {
            DgvMaestros.DataSource = _maestroManejador.ObtenerLista(filtro);
        }
        private void MaestrosEntrada_Load(object sender, EventArgs e)
        {
            BuscarMaestros("");
        }

        private void Btn_Maestros_EventDelete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de que deseas eliminar registro?", "Eliminar registro",
              MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarMaestros("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Eliminar()
        {
            string id = DgvMaestros.CurrentRow.Cells["n_control"].Value.ToString();
            _maestroManejador.Eliminar(id);
        }

        private void DgvMaestros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingMaestro();
            MaestrosFormulario frmMaestrosFormulario = new MaestrosFormulario(_maestro);
            frmMaestrosFormulario.ShowDialog();
            BuscarMaestros("");
        }

        private void BindingMaestro()
        {
            _maestro.NControl = DgvMaestros.CurrentRow.Cells["n_control"].Value.ToString();
            _maestro.NombreM = DgvMaestros.CurrentRow.Cells["nombrem"].Value.ToString();
            _maestro.ApellidopaternoM = DgvMaestros.CurrentRow.Cells["apellidopaternom"].Value.ToString();
            _maestro.ApellidomaternoM = DgvMaestros.CurrentRow.Cells["apellidomaternom"].Value.ToString();
            _maestro.Fechanacimiento = DgvMaestros.CurrentRow.Cells["f_nacimiento"].Value.ToString();
            _maestro.Estado = DgvMaestros.CurrentRow.Cells["estadom"].Value.ToString();
            _maestro.Ciudad = DgvMaestros.CurrentRow.Cells["ciudadm"].Value.ToString();
            _maestro.Sexo = DgvMaestros.CurrentRow.Cells["sexo"].Value.ToString();
            _maestro.Correo = DgvMaestros.CurrentRow.Cells["correo"].Value.ToString();
            _maestro.Tarjeta = DgvMaestros.CurrentRow.Cells["n_tarjeta"].Value.ToString();
        }

        private void Btn_Maestros_EventSearch(object sender, EventArgs e)
        {
            BuscarMaestros(TxtBuscar.Text);
        }

        private void Btn_Maestros_EventNew(object sender, EventArgs e)
        {
            MaestrosFormulario frmMaestrosFormulario = new MaestrosFormulario(_maestro);
            frmMaestrosFormulario.ShowDialog();
            BuscarMaestros("");
        }
    }
}

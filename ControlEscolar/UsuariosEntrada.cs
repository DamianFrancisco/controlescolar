﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class UsuariosEntrada : Form
    {
        UsuariosManejador _usuarioManejador;
        Usuarios _usuario;
        public UsuariosEntrada()
        {
            InitializeComponent();
            _usuarioManejador = new UsuariosManejador();
            _usuario = new Usuarios(); //instancia
        }

        private void BuscarUsuarios(string filtro)
        {
            DgvUsuarios.DataSource = _usuarioManejador.ObtenerLista(filtro);
        }
    
        private void UsuariosEntrada_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
        }

        private void BtnUsuarios_EventDelete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de que deseas eliminar registro?", "Eliminar registro",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Eliminar()
        {
            int id = Convert.ToInt32(DgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuarioManejador.Eliminar(id);
        }

        private void BtnUsuarios_EventNew(object sender, EventArgs e)
        {
            UsuariosFormulario frmUsuariosFormulario = new UsuariosFormulario();
            frmUsuariosFormulario.ShowDialog();
            BuscarUsuarios("");
        }

        private void DgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingUsuario();
            UsuariosFormulario frmUsuariosFormulario = new UsuariosFormulario(_usuario);
            frmUsuariosFormulario.ShowDialog();
            BuscarUsuarios("");
        }

        private void BindingUsuario()
        {
            _usuario.IdUsuario = Convert.ToInt32(DgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuario.Nombre = DgvUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
            _usuario.ApellidoPaterno = DgvUsuarios.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            _usuario.ApellidoMaterno = DgvUsuarios.CurrentRow.Cells["apellidomaterno"].Value.ToString();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //BuscarUsuarios(TxtBuscar.Text); se puede utilizar tambien pero lo buscaria por medio del text
        }

        private void BtnUsuarios_EventSearch(object sender, EventArgs e)
        {
            BuscarUsuarios(TxtBuscar.Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;
using Extensions.ControlEscolar;

namespace ControlEscolar
{
    public partial class EscuelasFormulario : Form
    {
        private OpenFileDialog _dialogCargarLogo;
        private EscuelasManejador _escuelaManejador;
        private bool _isEnableBinding = false;
        private bool _isImagenClear = false;
        private Escuelas _escuela;
        private RutasManager _rutasManager;
        public EscuelasFormulario()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutasManager = new RutasManager(Application.StartupPath);
            _escuelaManejador = new EscuelasManejador(_rutasManager);
            _escuela = new Escuelas();

            _escuela = _escuelaManejador.GetEscuela();

            if (!string.IsNullOrEmpty(_escuela.Idescuela.ToString()))
            {
                LoadEntity();
            }

            _isEnableBinding = true;
        }

        private void EscuelasFormulario_Load(object sender, EventArgs e)
        {

        }
        private void LoadEntity()
        {
            TxtNombre.Text = _escuela.Nombre;
            TxtDirector.Text = _escuela.Director;
            picLogo.ImageLocation = null;

            if (!string.IsNullOrEmpty(_escuela.Logo) && string.IsNullOrEmpty(_dialogCargarLogo.FileName))
            {
                picLogo.ImageLocation = _rutasManager.RutaLogoEscuela(_escuela);
            }
        }

        private void BindEntity()
        {
            if (_isEnableBinding)
            {
                _escuela.Nombre = TxtNombre.Text;
                _escuela.Director = TxtDirector.Text;
            }
        }
        private void CargarLogo()
        {
            _dialogCargarLogo.Filter = "imagen tipo(*.png)|*.png|Imagen tipo(*.jpg)|*.jpg";
            _dialogCargarLogo.Title = "Cargar un archivo de imagen";
            _dialogCargarLogo.ShowDialog();

            if (_dialogCargarLogo.FileName != "")
            {
                if (_escuelaManejador.CargarLogo(_dialogCargarLogo.FileName))
                {
                    picLogo.ImageLocation = _dialogCargarLogo.FileName;
                    _isImagenClear = false;
                }
                else
                {
                    MessageBox.Show("No se pueden cargar imagenes mayores a 5mb");
                }
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {

        }

        private void BtnEscuelasS_EventCancel(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de salir sin guardar cambios", "salio", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void BtnEscuelasS_EventSave(object sender, EventArgs e)
        {
            Save();
        }
        private void Save()
        {
            try
            {
                if (picLogo.Image != null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);

                        if (!string.IsNullOrEmpty(_escuela.Logo))
                        {
                            _escuelaManejador.GuardarLogo(_dialogCargarLogo.FileName, 1);
                            _dialogCargarLogo.Dispose();
                        }
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty;
                }
                if (_isEnableBinding)
                {
                    _escuelaManejador.LimpiarDocumento(1, "png");
                    _escuelaManejador.LimpiarDocumento(1, "jpg");
                }

                _escuelaManejador.Guardar(_escuela);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error");
            }
        }

        private void BtnEscuelas_EventDelete(object sender, EventArgs e)
        {
             if (MessageBox.Show("Estas seguro de eliminar los cambios del logo", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                EliminarLogo();
            }
        }

        private void EliminarLogo()
        {
            picLogo.ImageLocation = null;
            _isEnableBinding = true;
        }


        private void BtnEscuelas_EventSuccess(object sender, EventArgs e)
        {
            CargarLogo();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class MateriasEntrada : Form
    {
        MateriasManejador _materiaManejador;
        Materias _materia;
        public MateriasEntrada()
        {
            InitializeComponent();
            _materiaManejador = new MateriasManejador();
            _materia = new Materias();
        }

        private void BuscarMaterias(string filtro)
        {
            DgvMaterias.DataSource = _materiaManejador.ObtenerLista(filtro);
        }
        private void MateriasEntrada_Load(object sender, EventArgs e)
        {
            BuscarMaterias("");
        }

        private void Eliminar()
        {
            string id = DgvMaterias.CurrentRow.Cells["codimateria"].Value.ToString();
            _materiaManejador.Eliminar(id);
        }

        private void BindingMateria()
        {
            _materia.Codigo = DgvMaterias.CurrentRow.Cells["codimateria"].Value.ToString();
            _materia.Materia = DgvMaterias.CurrentRow.Cells["nombrem"].Value.ToString();
            _materia.Carrera = DgvMaterias.CurrentRow.Cells["carrera"].Value.ToString();
            _materia.Semestre = DgvMaterias.CurrentRow.Cells["semestre"].Value.ToString();
            _materia.Creditos = Convert.ToInt32(DgvMaterias.CurrentRow.Cells["creditos"].Value);
            _materia.Rea = DgvMaterias.CurrentRow.Cells["rea"].Value.ToString();
            _materia.Reb = DgvMaterias.CurrentRow.Cells["reb"].Value.ToString();
        }

        private void DgvMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingMateria();
            MateriasFormulario frmMateriasFormulario = new MateriasFormulario();
            frmMateriasFormulario.ShowDialog();
            BuscarMaterias("");
        }

        private void Btn_Materias_EventDelete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de que deseas eliminar registro?", "Eliminar registro",
              MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarMaterias("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Btn_Materias_EventNew(object sender, EventArgs e)
        {
            MateriasFormulario frmMateriasFormulario = new MateriasFormulario();
            frmMateriasFormulario.ShowDialog();
            BuscarMaterias("");
        }

        private void Btn_Materias_EventSearch(object sender, EventArgs e)
        {
            BuscarMaterias(TxtBuscar.Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;
using Extensions.ControlEscolar;

namespace ControlEscolar
{
    public partial class EscuelasEntrada : Form
    {
        EscuelasManejador _escuelaManejador;
        Escuelas _escuela;
        RutasManager _rutasManager;
        public EscuelasEntrada()
        {
            InitializeComponent();
            _escuelaManejador = new EscuelasManejador(_rutasManager);
            _escuela = new Escuelas();
        }

        private void BuscarEscuelas()
        {
            DgvEscuelas.DataSource = _escuelaManejador.GetEscuela();
        }
        private void EscuelasEntrada_Load(object sender, EventArgs e)
        {
            BuscarEscuelas();
        }

        private void Eliminar()
        {
            int id = Convert.ToInt32(DgvEscuelas.CurrentRow.Cells["idescuela"].Value);
            _escuelaManejador.Eliminar(id);
        }

        private void BindingEscuelas()
        {
            _escuela.Idescuela = Convert.ToInt32(DgvEscuelas.CurrentRow.Cells["idescuela"].Value);
            _escuela.Nombre = DgvEscuelas.CurrentRow.Cells["nombre"].Value.ToString();
            _escuela.Numero = Convert.ToInt32(DgvEscuelas.CurrentRow.Cells["numeroex"].Value);
            _escuela.Estadoe = DgvEscuelas.CurrentRow.Cells["estadoe"].Value.ToString();
            _escuela.Municipioe = DgvEscuelas.CurrentRow.Cells["municipioe"].Value.ToString();
            _escuela.Telefono = DgvEscuelas.CurrentRow.Cells["telefono"].Value.ToString();
            _escuela.Pagina = DgvEscuelas.CurrentRow.Cells["paginaw"].Value.ToString();
            _escuela.Email = DgvEscuelas.CurrentRow.Cells["emailp"].Value.ToString();
            _escuela.Director = DgvEscuelas.CurrentRow.Cells["director"].Value.ToString();
            _escuela.Logo = DgvEscuelas.CurrentRow.Cells["logo"].Value.ToString();
        }

        private void DgvEscuelas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingEscuelas();
            EscuelasFormulario frmEscuelasFormulario = new EscuelasFormulario();
            frmEscuelasFormulario.ShowDialog();
            BuscarEscuelas();
        }

        private void Btn_Escuelas_EventDelete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de que deseas eliminar registro?", "Eliminar registro",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarEscuelas();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Btn_Escuelas_EventNew(object sender, EventArgs e)
        {
            EscuelasFormulario frmEscuelasFormulario = new EscuelasFormulario();
            frmEscuelasFormulario.ShowDialog();
            BuscarEscuelas();
        }

        private void Btn_Escuelas_EventSearch(object sender, EventArgs e)
        {
            //BuscarEscuelas(TxtBuscar.Text);
        }
    }
}

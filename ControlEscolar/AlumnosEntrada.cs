﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class AlumnosEntrada : Form
    {
        AlumnosManejador _alumnoManejador;
        Alumnos _alumno;
        public AlumnosEntrada()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnosManejador();
            _alumno = new Alumnos();
        }

        private void BuscarAlumnos(string filtro)
        {
            DgvAlumnos.DataSource = _alumnoManejador.ObtenerLista(filtro);
        }

        private void AlumnosEntrada_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
        }

        private void Eliminar()
        {
            string id = DgvAlumnos.CurrentRow.Cells["numero_control"].Value.ToString();
            _alumnoManejador.Eliminar(id);
        }

        private void BindingAlumno()
        {
            _alumno.NumeroControl = DgvAlumnos.CurrentRow.Cells["numero_control"].Value.ToString();
            _alumno.Nombre = DgvAlumnos.CurrentRow.Cells["nombre"].Value.ToString();
            _alumno.Apellido_Paterno = DgvAlumnos.CurrentRow.Cells["apellido_paterno"].Value.ToString();
            _alumno.Apellido_Materno = DgvAlumnos.CurrentRow.Cells["apellido_materno"].Value.ToString();
            _alumno.Sexo = DgvAlumnos.CurrentRow.Cells["sexo"].Value.ToString();
            _alumno.Fecha_Nacimiento = DgvAlumnos.CurrentRow.Cells["fecha_nacimiento"].Value.ToString();
            _alumno.Correo_Electronico = DgvAlumnos.CurrentRow.Cells["correo_electronico"].Value.ToString();
            _alumno.Telefono_Contacto = DgvAlumnos.CurrentRow.Cells["telefono_contacto"].Value.ToString();
            _alumno.Estado = DgvAlumnos.CurrentRow.Cells["estado"].Value.ToString();
            _alumno.Municipio = DgvAlumnos.CurrentRow.Cells["municipio"].Value.ToString();
            _alumno.Domicilio = DgvAlumnos.CurrentRow.Cells["domicilio"].Value.ToString();
        }

        private void DgvAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            AlumnosFormulario frmAlumnosFormulario = new AlumnosFormulario(_alumno);
            frmAlumnosFormulario.ShowDialog();
            BuscarAlumnos("");
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            //BuscarAlumnos(TxtBuscar.Text);
        }

        private void Btn_Alumnos_EventDelete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de que deseas eliminar registro?", "Eliminar registro",
               MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Btn_Alumnos_EventNew(object sender, EventArgs e)
        {
            AlumnosFormulario frmAlumnosFormulario = new AlumnosFormulario();
            frmAlumnosFormulario.ShowDialog();
            BuscarAlumnos("");
        }

        private void Btn_Alumnos_EventSearch(object sender, EventArgs e)
        {
            BuscarAlumnos(TxtBuscar.Text);
        }
    }
}

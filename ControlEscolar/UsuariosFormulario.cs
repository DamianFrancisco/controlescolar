﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaDeNegocio.ControlEscolar;
using Entidades.ControlEscolar;

namespace ControlEscolar
{
    public partial class UsuariosFormulario : Form
    {
        private UsuariosManejador _usuarioManejador;
        private Usuarios _usuario;
        private bool _isEnableBinding = false;
        public UsuariosFormulario()
        {
            InitializeComponent();
            _usuarioManejador = new UsuariosManejador();
            _usuario = new Usuarios();
            _isEnableBinding = true;
            BindingUsuario(); //Actualiza datos 
        }

        public UsuariosFormulario(Usuarios usuario)
        {
            //Se esta haciendo otro constructor para mandar llamar los datos que se estan
            //agregando y guardando los mande al datagrid automaticamente
            InitializeComponent();
            _usuarioManejador = new UsuariosManejador();
            _usuario = new Usuarios();
            _usuario = usuario;
            BindingUsuarioReload();
            _isEnableBinding = true; //habilitar las variables  
        }
        private void BindingUsuarioReload()
        {
            TxtNombre.Text = _usuario.Nombre;
            TxtApellidoPaterno.Text = _usuario.ApellidoPaterno;
            TxtApellidoMaterno.Text = _usuario.ApellidoMaterno;
        }
        private void UsuariosFormulario_Load(object sender, EventArgs e)
        {

        }

        private void BtnUsuariosS_EventSave(object sender, EventArgs e)
        {
            BindingUsuario();

            if (ValidarUsuario())
            {

                Guardar();
                MessageBox.Show("Guardado Correctamente!!");
                this.Close();
            }
        }
        private void Guardar()
        {
            _usuarioManejador.Guardar(_usuario);
        }

        private bool ValidarUsuario()
        {
            var res = _usuarioManejador.EsusuarioValido(_usuario);

            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        
        private void BindingUsuario()
        {
            if (_isEnableBinding)
            {
                if (_usuario.IdUsuario == 0)
                {
                    _usuario.IdUsuario = 0;
                }

                _usuario.Nombre = TxtNombre.Text;
                _usuario.ApellidoPaterno = TxtApellidoPaterno.Text;
                _usuario.ApellidoMaterno = TxtApellidoMaterno.Text;
            }
        }

        private void TxtNombre_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void TxtApellidoPaterno_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void TxtApellidoMaterno_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void BtnUsuariosS_EventCancel(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;

namespace Extensions.ControlEscolar
{
    public class RutasManager
    {
        private string _appPath;

        private const string CERTIFICADOS = "certificados";

        private const string LOGOS = "logo";

        public RutasManager(string appPath)
        {
            _appPath = appPath;
        }

        public string RutaRepositorioCertificados
        {
            get { return Path.Combine(_appPath, CERTIFICADOS); }
        }

        public string RutaRepositoriosLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
        }

        public void CrearRepositoriosCertificados()
        {
            if (!Directory.Exists(RutaRepositorioCertificados))
            {
                Directory.CreateDirectory(RutaRepositorioCertificados);
            }
        }

        public void CrearRepositoriosLogos()
        {
            if (!Directory.Exists(RutaRepositoriosLogos))
            {
                Directory.CreateDirectory(RutaRepositoriosLogos);
            }
        }

        public void CrearRepositoriosCertificadosEstudios(int empresaId)
        {
            CrearRepositoriosCertificados();
            string ruta = Path.Combine(RutaRepositorioCertificados, empresaId.ToString());

            if (!Directory.Exists(ruta))

            {

                Directory.CreateDirectory(ruta);

            }
        }

        public void CrearRepositoriosLogosEscuela(int escuelaid)
        {
            CrearRepositoriosLogos();

            string ruta = Path.Combine(RutaRepositoriosLogos, escuelaid.ToString());

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }

        public string RutaLogoEscuela(Escuelas escuela)
        {
            return Path.Combine(RutaRepositoriosLogos, escuela.Idescuela.ToString(), escuela.Logo);
        }
    }
}


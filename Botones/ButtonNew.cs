﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;//Agregar Referencia

namespace Botones
{
    public class ButtonNew : Button
    {
        public ButtonNew() : base()
        {
            this.Image = global::Botones.Properties.Resources.Nuevo;//Agrega la imagen
            this.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;//Posicion de la imagen
            this.Height = 50;
            this.Width = 150;
        }
    }
}

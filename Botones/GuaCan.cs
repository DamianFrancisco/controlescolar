﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Botones
{
    public partial class GuaCan : UserControl
    {
        public event EventHandler EventSave;
        public event EventHandler EventCancel;
        public GuaCan()
        {
            InitializeComponent();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlButton(true, false);

            if (EventSave != null)
            {
                EventSave(sender, e);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlButton(false, true);

            if (EventCancel != null)
            {
                EventCancel(sender, e);
            }
        }

        public void ControlButton(Boolean Save, Boolean Cancel)
        {
            BtnGuardar.Enabled = Save;
            BtnCancelar.Enabled = Cancel;
        }

        public string TextButtonSave
        {
            get
            {
                return BtnGuardar.Text;
            }

            set
            {
                BtnGuardar.Text = value;
            }
        }
        public string TextButtonCancel
        {
            get
            {
                return BtnCancelar.Text;
            }

            set
            {
                BtnCancelar.Text = value;
            }
        }

        private void GuaCan_Load(object sender, EventArgs e)
        {
            ControlButton(true, true);
        }
    }
}

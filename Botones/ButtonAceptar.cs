﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Botones
{
    public class ButtonAceptar : Button
    {
        public ButtonAceptar() : base()
        {
            this.Image = global::Botones.Properties.Resources.aceptar;//Agrega la imagen
            this.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;//Posicion de la imagen
            this.Height = 50;
            this.Width = 150;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Botones
{
    public partial class SelDel : UserControl
    {
        public event EventHandler EventSuccess;
        public event EventHandler EventDelete;
        
        public SelDel()
        {
            InitializeComponent();
        }

        private void SelDel_Load(object sender, EventArgs e)
        {
            ControlButton(true, true);
        }

        public void ControlButton(Boolean Success, Boolean Delete)
        {
            BtnSeleccionar.Enabled = Success;
            BtnEliminar.Enabled = Delete;
        }

        public string TextButtonSuccess
        {
            get
            {
                return BtnSeleccionar.Text;
            }

            set
            {
                BtnSeleccionar.Text = value;
            }
        }
        public string TextButtonDelete
        {
            get
            {
                return BtnEliminar.Text;
            }

            set
            {
                BtnEliminar.Text = value;
            }
        }

        private void BtnSeleccionar_Click(object sender, EventArgs e)
        {
            ControlButton(true, false);

            if (EventSuccess != null)
            {
                EventSuccess(sender, e);
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            ControlButton(false, true);

            if (EventDelete != null)
            {
                EventDelete(sender, e);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Botones
{
    public partial class BusEliNue : UserControl
    {
        public event EventHandler EventSearch;
        public event EventHandler EventDelete;//Crearevento para los botones
        public event EventHandler EventNew;

        public BusEliNue()
        {
            InitializeComponent();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            ControlButton(true,true,true);

            if (EventDelete != null)
            {
                EventDelete(sender, e);
            }
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlButton(false,true,true);

            if (EventNew != null)
            {
                EventNew(sender, e);
            }
        }

        public void ControlButton(Boolean Search ,Boolean Delete, Boolean New)
        {
            BtnBuscar.Enabled = Search;
            BtnEliminar.Enabled = Delete;
            BtnNuevo.Enabled = New;
        }

        public string TextButtonSearch
        {
            get
            {
                return BtnBuscar.Text;
            }

            set
            {
                BtnBuscar.Text = value;
            }
        }
        public string TextButtonDelete
        {
            get
            {
                return BtnEliminar.Text;
            }

            set
            {
                BtnEliminar.Text = value;
            }
        }

        public string TextButtonNew
        {
            get
            {
                return BtnNuevo.Text;
            }

            set
            {
                BtnNuevo.Text = value;
            }
        }

        private void EliNue_Load(object sender, EventArgs e)
        {
            ControlButton(true, true, true);
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            ControlButton(true, true, false);

            if (EventSearch != null)
            {
                EventSearch(sender, e);
            }
        }
    }
}

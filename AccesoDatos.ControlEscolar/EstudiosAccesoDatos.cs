﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;
using Conexionbd;


namespace AccesoDatos.ControlEscolar
{
    public class EstudiosAccesoDatos
    {
        Conexion _conexion;

        public EstudiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(string idEstudio)
        {
            string cadena = string.Format("Delete from estudios where idestudio='{0}'", idEstudio);
            _conexion.EjecutarConsulta(cadena);

        }

        public void Guardar(Estudios estudio)
        {
            if (estudio.Idestudio  == 0)
            {
                string cadena = string.Format("Insert into estudios values(null, '{0}')", estudio.Archivo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update estudios set archivo= '{0}'" + "where idestudio='{1}'",
                    estudio.Archivo, estudio.Idestudio);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public List<Estudios> ObtenerLista(string filtro)
        {
            var list = new List<Estudios>();

            string consulta = string.Format("Select * from estudios where archivo like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "estudios");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estudio = new Estudios
                {
                    Idestudio = Convert.ToInt32(row["idestudio"]),
                    Archivo = row["archivo"].ToString(),
                };
                list.Add(estudio);
            }
            return list;
        }

    }
}

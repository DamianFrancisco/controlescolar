﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;
using Conexionbd;


namespace AccesoDatos.ControlEscolar
{
    public class MunicipiosAccesoDatos
    {
        Conexion _conexion;

        public MunicipiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();

            string consulta = string.Format("select * from municipios  where fkestado='{0}'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "municipios");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var municipio = new Municipios
                {
                    IdMunicipio = Convert.ToInt32(row["id_municipio"]),
                    Nombre = row["nombre"].ToString(),
                    Fkestado = row["fkestado"].ToString()
                };
                list.Add(municipio);
            }
            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using Conexionbd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MateriasAccesoDatos
    {
        Conexion _conexion;

        public MateriasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(string Codigo)
        {
            string cadena = string.Format("Delete from materias where codimateria={0}", Codigo);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Materias materia)
        {
            if (materia.Codigo != "")
            {
                string cadena = string.Format("Insert into materias values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                    materia.Codigo, materia.Materia, materia.Carrera, materia.Semestre, materia.Creditos, materia.Rea, materia.Reb);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update materias set codimateria= '{0}', nombrem= '{1}', carrera= '{2}', " +
                    "semestre= '{3}', creditos= '{4}', rea= '{5}', reb= '{6}'",
                    materia.Codigo, materia.Materia, materia.Carrera, materia.Semestre, materia.Creditos, materia.Rea, materia.Reb);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public List<Materias> ObtenerLista(string filtro)
        {
            var list = new List<Materias>();

            string consulta = string.Format("Select * from materias where codimateria like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "materias");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materia = new Materias
                {
                    Codigo = row["codimateria"].ToString(),
                    Materia = row["nombrem"].ToString(),
                    Carrera = row["carrera"].ToString(),
                    Semestre = row["semestre"].ToString(),
                    Creditos = Convert.ToInt32(row["creditos"]),
                    Rea = row["rea"].ToString(),
                    Reb = row["reb"].ToString()
                };
                list.Add(materia);
            }
            return list;
        }
    }
}

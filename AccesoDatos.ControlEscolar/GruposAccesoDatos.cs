﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using Conexionbd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GruposAccesoDatos
    {
        Conexion _conexion;

        public GruposAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(int idGrupo)
        {
            string cadena = string.Format("Delete from grupos where idgrupo={0}", idGrupo);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Grupos grupo)
        {
            if (grupo.Idgrupo == 0)
            {
                string cadena = string.Format("Insert into grupos values('{0}','{1}','{2}','{3}')",
                    grupo.Grupo, grupo.Alumnos, grupo.Carrera, grupo.Materias);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update grupos set grupo= '{0}', alumnos= '{1}', carrera= '{2}', materias= '{3}'"
                    + "where idgrupo='{4}'", grupo.Grupo, grupo.Alumnos, grupo.Carrera, grupo.Materias, grupo.Idgrupo);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public List<Grupos> ObtenerLista(string filtro)
        {
            var list = new List<Grupos>();

            string consulta = string.Format("Select * from grupos where idgrupo like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "grupos");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupos
                {
                    Idgrupo = Convert.ToInt32(row["idgrupo"]),
                    Grupo = row["grupo"].ToString(),
                    Alumnos = row["alumnos"].ToString(),
                    Carrera = row["carrera"].ToString(),
                    Materias = row["materias"].ToString()
                };
                list.Add(grupo);
            }
            return list;
        }
    }
}

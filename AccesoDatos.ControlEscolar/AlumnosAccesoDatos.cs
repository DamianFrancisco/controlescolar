﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;
using Conexionbd;


namespace AccesoDatos.ControlEscolar
{
    public class AlumnosAccesoDatos
    {
        Conexion _conexion;

        public AlumnosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(string numero_control)
        {
            string cadena = string.Format("Delete from alumnos where numero_control={0}", numero_control);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Alumnos alumno)
        {
            if (alumno.NumeroControl != "")
            {
                string cadena = string.Format("Insert into alumnos values('{0}','{1}','{2}','{3}','{4}','{5}','{6}'" +
                    ",'{7}','{8}','{9}','{10}')",
                    alumno.NumeroControl, alumno.Nombre, alumno.Apellido_Paterno, alumno.Apellido_Materno,
                    alumno.Sexo, alumno.Fecha_Nacimiento, alumno.Correo_Electronico, alumno.Telefono_Contacto,
                    alumno.Estado, alumno.Municipio, alumno.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update alumnos set numero_control= '{0}', nombre= '{1}', apellido_paterno= '{2}', " +
                    "apellido_materno= '{3}', sexo= '{4}', fecha_nacimiento= '{5}', correo_electronico= '{6}', telefono_contacto= '{7}'," +
                    "estado= '{8}', municipio= '{9}', domicilio= '{10}'",
                    alumno.NumeroControl, alumno.Nombre, alumno.Apellido_Paterno, alumno.Apellido_Materno,
                    alumno.Sexo, alumno.Fecha_Nacimiento, alumno.Correo_Electronico, alumno.Telefono_Contacto,
                    alumno.Estado, alumno.Municipio, alumno.Domicilio);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();

            string consulta = string.Format("Select * from alumnos where numero_control like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumnos");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumnos
                {
                    NumeroControl = row["numero_control"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellido_Paterno = row["apellido_paterno"].ToString(),
                    Apellido_Materno = row["apellido_materno"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Fecha_Nacimiento = row["fecha_nacimiento"].ToString(),
                    Correo_Electronico = row["correo_electronico"].ToString(),
                    Telefono_Contacto = row["telefono_contacto"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString(),
                    Domicilio = row["domicilio"].ToString()
                };
                list.Add(alumno);
            }
            return list;
        }
    }
}

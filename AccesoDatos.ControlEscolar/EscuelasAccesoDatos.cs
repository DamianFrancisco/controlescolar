﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar;
using Conexionbd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelasAccesoDatos
    {
        Conexion _conexion;

        public EscuelasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(int idEscuela)
        {
            string cadena = string.Format("Delete from escuelas where idescuela={0}", idEscuela);
            _conexion.EjecutarConsulta(cadena);

        }
        public void Guardar(Escuelas escuela)
        {
            if (escuela.Idescuela == 0)
            {
                string cadena = string.Format("Insert into escuelas values(null,'{0}','{1}','{2}, '{3}','{4}','{5}','{6}','{7}',{8}')",
                    escuela.Nombre, escuela.Numero, escuela.Estadoe, escuela.Municipioe, escuela.Telefono, escuela.Pagina,
                    escuela.Email, escuela.Director, escuela.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update escuelas set nombre= '{0}', numeroex= '{1}', estadoe= '{2}', municipioe= '{3}'," +
                    "telefono= '{4}', paginaw= '{5}', emailp='{6}' director= '{7}', logo= '{8}'" + "where idescuela='{9}'",
                    escuela.Nombre, escuela.Numero, escuela.Estadoe, escuela.Municipioe, escuela.Telefono, escuela.Pagina,
                    escuela.Email, escuela.Director, escuela.Logo, escuela.Idescuela);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public Escuelas GetEscuela()
        {
            var ds = new DataSet();
            string consulta = "select * from escuelas";
            ds = _conexion.ObtenerDatos(consulta, "escuelas");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela = new Escuelas();
            foreach (DataRow row in dt.Rows)
            {
                escuela.Idescuela = Convert.ToInt32(row["idescuela"]);
                escuela.Nombre = row["nombre"].ToString();
                escuela.Numero = Convert.ToInt32(row["numeroex"]);
                escuela.Estadoe = row["estadoe"].ToString();
                escuela.Municipioe = row["municipioe"].ToString();
                escuela.Telefono = row["telefono"].ToString();
                escuela.Pagina = row["paginaw"].ToString();
                escuela.Email = row["emailp"].ToString();
                escuela.Director = row["director"].ToString();
                escuela.Logo = row["logo"].ToString();
            }
            return escuela;
        }
    }
}

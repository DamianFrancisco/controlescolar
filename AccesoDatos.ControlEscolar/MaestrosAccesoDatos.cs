﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar;
using Conexionbd;

namespace AccesoDatos.ControlEscolar
{
    public class MaestrosAccesoDatos
    {
        Conexion _conexion;

        public MaestrosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public void Eliminar(string ncontrol)
        {
            string cadena = string.Format("Delete from maestros where n_control='{0}'", ncontrol);
            _conexion.EjecutarConsulta(cadena);

        }

        public void Guardar(Maestros maestro)
        {
            if (maestro.NControl != "")
            {
                string cadena = string.Format("Insert into maestros values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'," +
                    "'{8}','{9}')", maestro.NControl, maestro.NombreM, maestro.ApellidopaternoM, maestro.ApellidomaternoM
                    , maestro.Fechanacimiento, maestro.Estado, maestro.Ciudad, maestro.Sexo, maestro.Correo, maestro.Tarjeta);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update maestros set nombrem= '{0}', apellidopaternom= '{1}', apellidomaternom= '{2}'," +
                    "f_nacimiento= '{3}, estadom= '{4}', ciudadm= '{5}', sexo= ´{6}', correo= '{7}', tarjeta= '{8}'" + "where n_control='{9}'",
                    maestro.NombreM, maestro.ApellidopaternoM, maestro.ApellidomaternoM, maestro.Fechanacimiento,
                    maestro.Estado, maestro.Ciudad, maestro.Sexo, maestro.Correo, maestro.Tarjeta, maestro.NControl);
                _conexion.EjecutarConsulta(cadena);
            }
        }

        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();

            string consulta = string.Format("Select * from maestros where nombrem like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestros");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var maestro = new Maestros
                {
                    NControl = row["n_control"].ToString(),
                    NombreM = row["nombrem"].ToString(),
                    ApellidopaternoM = row["apellidopaternom"].ToString(),
                    ApellidomaternoM = row["apellidomaternom"].ToString(),
                    Fechanacimiento = row["f_nacimiento"].ToString(),
                    Estado = row["estadom"].ToString(),
                    Ciudad = row["ciudadm"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Correo = row["correo"].ToString(),
                    Tarjeta = row["n_tarjeta"].ToString(),
                };
                list.Add(maestro);
            }
            return list;
        }
    }
}
